# Задача на последовательный поиск.

# Task 

Задача:
В последовательности неотрицательных целых чисел не превышающих 10000, размером 1000 элементов найти число R удовлетворяющие следующим условиям:
R максимально;
R кратно 14;
R произведение 2х различных элементов последовательности;
если такого числа нет вывести -1

# Solution

```
using System;

namespace LW2
{
	class Program
	{
		static void Main(string[] args)
		{
			{
				// объявление массива со случайными значениями.. (всего значений 1001)
				Random random = new Random();
				int[] array = new int[1000]; 

				// заполнениее массива
				for (int i = 0; i < 1000; i++) 
					array[i] = random.Next(0, 10000);
				
				// поиск максимума
				int R = array[0];
			
				for (int i = 0; i < array.Length; i++) 
					if (array[i] > R) 
						R = array[i];

				//<TODO> : проверка на кратность 14. Если нет, вывод -1.

				var a = array[new Random().Next(0, array.Length)] * array[new Random().Next(0, array.Length)];

				if (R % 14 == 0 && R == a)
					Console.WriteLine(R);
				else Console.WriteLine("-1"); 
				
				Console.ReadKey();
			}
		}
	}
}

```